import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Transaction from './screen/Transaction';
import Checkout from './screen/Checkout';
import FormPesanan from './screen/FormPesanan';
import KodePromo from './screen/KodePromo';
import Promo from './screen/Promo';

const Stack = createStackNavigator();

export default function TransactionNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Transaction" component={Transaction} />
      <Stack.Screen name="Checkout" component={Checkout} />
      <Stack.Screen name="FormPesanan" component={FormPesanan} />
      <Stack.Screen name="KodePromo" component={KodePromo} />
      <Stack.Screen name="Promo" component={Promo} />
    </Stack.Navigator>
  );
}
