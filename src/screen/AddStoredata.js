import React, {useState} from 'react';
import {
  StyleSheet,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

export default function AddStoreData({navigation}) {
  const data = [{text: 'Buka'}, {text: 'Tutup'}];
  const [open, setOpen] = useState('');
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <View style={styles.Header}>
            <Text style={styles.TextDes}>Add Store Data</Text>
          </View>
          <View style={{height: 50}} />
          <View>
            <Text style={styles.InputText}>Nama Toko</Text>
            <TextInput style={styles.TextInput} />
          </View>
          <View style={{height: 20}} />
          <View>
            <Text style={styles.InputText}>Alamat</Text>
            <TextInput style={styles.TextInput} />
          </View>
          <View style={{height: 20}} />
          <View>
            <Text style={styles.InputText}>Status Buka Tutup </Text>
            <View style={{height: 20}} />
            <View style={{flexDirection: 'row', paddingHorizontal: 5}}>
              {data.map((value, index) => (
                <View
                  style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    width: 200,
                  }}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <TouchableOpacity
                      activeOpacity={0.8}
                      style={styles.Tick}
                      onPress={() => {
                        if (open === value.text) {
                          setOpen('');
                        } else {
                          setOpen(value.text);
                        }
                      }}>
                      {open === value.text ? (
                        <Image
                          source={require('../assets/Icon/Tick.png')}
                          style={{height: 20, width: 20, resizeMode: 'contain'}}
                        />
                      ) : null}
                    </TouchableOpacity>
                    <View style={{width: 10}} />
                    <Text
                      style={{
                        color: 'black',
                        fontWeight: '400',
                        fontSize: 12,
                      }}>
                      {value.text}
                    </Text>
                  </View>
                  <View style={{width: 30}} />
                </View>
              ))}
            </View>
          </View>
          <View style={{height: 30}} />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{alignItems: 'center'}}>
              <Text style={styles.InputText}> Jam Buka</Text>
              <View style={{height: 10}} />
              <TextInput
                style={{
                  width: 150,
                  height: 45,
                  borderRadius: 8,
                  backgroundColor: '#F6F8FF',
                  paddingHorizontal: 10,
                }}
              />
            </View>
            <View style={{alignItems: 'center'}}>
              <Text style={styles.InputText}>Jam Tutup</Text>
              <View style={{height: 10}} />
              <TextInput
                style={{
                  width: 150,
                  height: 45,
                  borderRadius: 8,
                  backgroundColor: '#F6F8FF',
                  paddingHorizontal: 10,
                }}
              />
            </View>
          </View>
          <View style={{height: 20}} />
          <View>
            <Text style={styles.InputText}>Jumlah Rating</Text>
            <View style={{height: 10}} />
            <TextInput style={styles.TextInput} />
          </View>
          <View style={{height: 20}} />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{alignItems: 'center'}}>
              <Text style={styles.InputText}>Harga Minimal</Text>
              <TextInput
                style={{
                  width: 180,
                  height: 45,
                  borderRadius: 8,
                  backgroundColor: '#F6F8FF',
                  paddingHorizontal: 10,
                }}
              />
            </View>
            <View style={{alignItems: 'center'}}>
              <Text style={styles.InputText}>Harga Maksimal</Text>
              <TextInput
                style={{
                  width: 180,
                  height: 45,
                  borderRadius: 8,
                  backgroundColor: '#F6F8FF',
                  paddingHorizontal: 10,
                }}
              />
            </View>
          </View>
          <View style={{height: 20}} />
          <View>
            <Text style={styles.InputText}>Deskripsi</Text>
            <TextInput style={styles.TextInput} />
          </View>
          <View style={{height: 20}} />
          <View>
            <Text style={styles.InputText}>Gambar Toko</Text>
            <TouchableOpacity
              activeOpacity={0.8}
              style={{
                marginTop: 15,
                height: 45,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
            />
          </View>
          <View style={{height: 50}} />
          <TouchableOpacity
            style={{
              width: 335,
              height: 50,
              marginTop: 30,
              backgroundColor: '#BB2427',
              borderRadius: 8,
              paddingVertical: 15,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Simpan
            </Text>
          </TouchableOpacity>
          <View style={{height: 10}} />
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 15,
  },
  Header: {
    width: '100%',
    paddingTop: 24,
    paddingBottom: 17,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  Body: {
    borderTopWidth: 1,
    borderTopColor: '#EEEEEE',
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingHorizontal: 20,
  },
  TickView: {
    marginTop: 43,
    flexDirection: 'row',
    alignItems: 'center',
  },
  TickView2: {
    marginTop: 23,
    flexDirection: 'row',
    alignItems: 'center',
  },
  Tick: {
    width: 24,
    height: 24,
    borderWidth: 1,
    borderColor: '#B8B8B8',
  },
  TickText: {
    marginLeft: 23,
    color: '#201F26',
    fontWeight: '400',
    fontSize: 14,
  },
  Box: {
    marginTop: 27,
    height: 84,
    width: 84,
    borderWidth: 1,
    borderColor: '#BB2427',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Eikon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#000000',
  },
  Camera: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#BB2427',
  },
  TextCamera: {
    marginTop: 5,
    color: '#BB2427',
    fontFamily: 'Montserrat',
    fontWeight: '400',
    fontSize: 12,
  },
  InputText: {
    color: 'black',
    fontWeight: 'bold',
  },
  InputText2: {
    color: 'red',
    fontWeight: 'bold',
    marginTop: 27,
  },
  TextInput: {
    marginTop: 15,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 10,
  },
  TextInput2: {
    marginTop: 15,
    width: '100%',
    height: '20%',
    borderRadius: 8,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 10,
  },
  TextDes: {
    marginLeft: 20,
    fontSize: 18,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#201F26',
  },
});
