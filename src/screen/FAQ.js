import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';

export default function FAQ({navigation, route}) {
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={styles.Eikon}
              source={require('../assets/Icon/Back.png')}
            />
          </TouchableOpacity>
          <Text style={styles.TextDes}>FAQ</Text>
        </View>

        <View style={styles.Body}>
          <View style={{position: 'relative'}}>
            <TouchableOpacity
              style={{position: 'absolute', top: '50%', left: '98%'}}>
              <Image
                style={{
                  width: 24,
                  height: 24,
                  resizeMode: 'contain',
                }}
                source={require('../assets/Icon/arrow.png')}
              />
            </TouchableOpacity>

            <Text style={{fontSize: 14, fontWeight: '500', color: 'black'}}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit?
            </Text>
          </View>

          <Text
            style={{
              fontSize: 14,
              fontWeight: '500',
              color: '#595959',
              marginTop: 20,
            }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit?
          </Text>
        </View>
        <Question />
        <Question />
        <Question />
        <Question />
        <Question />
      </ScrollView>
    </View>
  );
}

const Question = () => {
  return (
    <View>
      <View style={styles.Body}>
        <View style={{position: 'relative', marginBottom: 20}}>
          <TouchableOpacity
            style={{position: 'absolute', top: '50%', left: '98%'}}>
            <Image
              style={{
                width: 24,
                height: 24,
                resizeMode: 'contain',
              }}
              source={require('../assets/Icon/arrowDown.png')}
            />
          </TouchableOpacity>
          <Text style={{fontSize: 14, fontWeight: '500', color: 'black'}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit?
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
  Header: {
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 17,
    paddingRight: 33,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    marginBottom: 5,
  },
  Eikon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#000000',
  },
  TextDes: {
    marginLeft: 20,
    fontSize: 18,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#201F26',
  },

  Body: {
    marginTop: 12,
    marginHorizontal: 17,
    backgroundColor: '#FFFFFF',
    paddingTop: 10,
    paddingBottom: 5,
    paddingHorizontal: 17,
    justifyContent: 'center',
  },
});
