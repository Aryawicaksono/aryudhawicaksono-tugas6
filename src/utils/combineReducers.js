import {combineReducers} from 'redux';
import authReducer from './reducers/authReducers';
import dataReducer from './reducers/dataReducers';

const rootReducer = combineReducers({
  auth: authReducer,
  data: dataReducer,
});

export default rootReducer;
