import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Profile from './screen/Profile';
import EditProfile from './screen/Editprofile';
import FAQ from './screen/FAQ';

const Stack = createStackNavigator();

export default function ProfileNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="FAQ" component={FAQ} />
    </Stack.Navigator>
  );
}
