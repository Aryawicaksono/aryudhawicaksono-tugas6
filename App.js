import React from 'react';
import AddStoreData from './src/screen/AddStoredata';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './src/utils/store';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StatusBar} from 'react-native';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar barStyle={'dark-content'} />
          {/* <Routing /> */}
          <AddStoreData />
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};
export default App;
